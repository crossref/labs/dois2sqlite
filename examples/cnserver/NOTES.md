## Crosscite docs

[https://citation.crosscite.org/docs.html](https://citation.crosscite.org/docs.html)

## Roque Scholar

https://github.com/front-matter/rogue-scholar-api/blob/main/api/utils.py

## Commonmeta

https://github.com/front-matter/commonmeta-py/tree/main/docs/writers

## CSL styles and locales retieved from here:

https://github.com/citation-style-language

chicago-note-bibliography
turabian-author-date
ieee
american-sociological-association
vancouver
harvard
