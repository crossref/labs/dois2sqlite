## Regenerate docs with

```
typer dois2sqlite/cli.py utils docs --name dois2sqlite --output README2.md
```

## Crosscite docs

[https://citation.crosscite.org/docs.html](https://citation.crosscite.org/docs.html)

## Roque Scholar

https://github.com/front-matter/rogue-scholar-api/blob/main/api/utils.py

## Commonmeta

https://github.com/front-matter/commonmeta-py/tree/main/docs/writers

## CSL styles and locales retieved from here:

https://github.com/citation-style-language

## Most popular citation formats

chicago-note-bibliography
turabian-author-date
ieee
american-sociological-association
vancouver
harvard

## Current API filters to consider supporting

has-abstract - [0 or 1], metadata for records with/without an abstract
has-affiliation - [0 or 1], metadata for records with/without affiliation information
has-archive - [0 or 1], metadata which includes/does not include name of archive partner
has-assertion - [0 or 1], metadata for records with/without assertions
has-authenticated-orcid - [0 or 1], metadata which includes/does not include one or more ORCIDs where the depositing publisher claims to have witness the ORCID owner authenticate with ORCID
has-award - [0 or 1], metadata for records with/without award
has-clinical-trial-number - [0 or 1], metadata for records with/without a clinical trial number
has-content-domain - [0 or 1], metadata where the publisher records/does not record a domain name location for Crossmark content
has-description
has-domain-restriction - [0 or 1], metadata where the publisher restricts/does not restrict Crossmark usage to content domains
has-event - [0 or 1], metadata for records with/without event
has-full-text - [0 or 1], metadata that includes/does not include any full text resource elements
has-funder - [0 or 1], metadata which includes/does not include one or more funder entry
has-funder-doi - [0 or 1], metadata for records with/without funder DOI
has-license - [0 or 1], metadata that includes/does not include any license_ref elements
has-orcid - [0 or 1], metadata which includes/does not include one or more ORCIDs
has-references - [0 or 1], metadata for works that have/don't have a list of references
has-relation - [0 or 1], metadata for records that either assert/do not assert or are/are not the object of a relation
has-ror-id - [0 or 1], metadata for records with/without ROR ID
has-update - [0 or 1], metadata for records with/without update information
has-update-policy - [0 or 1], metadata for records that include/do not include a link to an editorial update policy
is-update - [0 or 1], metadata for records that represent/do not represent editorial updates
